document: modulemd
version: 2
data:
    stream: 10
    summary: Javascript runtime
    description: >-
        Node.js is a platform built on Chrome's JavaScript runtime
        for easily building fast, scalable network applications.
        Node.js uses an event-driven, non-blocking I/O model that
        makes it lightweight and efficient, perfect for data-intensive
        real-time applications that run across distributed devices.
    license:
        module:
            - MIT
    dependencies:
        - buildrequires:
            platform: [el8]
          requires:
            platform: [el8]
    references:
        community: http://nodejs.org
        documentation: http://nodejs.org/en/docs
        tracker: https://github.com/nodejs/node/issues
    profiles:
        common:
            rpms:
                - nodejs
                - npm
        development:
            rpms:
                - nodejs
                - nodejs-devel
                - npm
        minimal:
            rpms:
                - nodejs
        s2i:
            rpms:
                - nodejs
                - npm
                - nodejs-nodemon
    api:
        rpms:
            - nodejs
            - nodejs-devel
            - npm
    buildopts:
        rpms:
            macros: |
                %_with_bootstrap 1
    components:
        rpms:
            nodejs:
                rationale: Javascript runtime and npm package manager.
                buildorder: 10
                ref: stream-10-rhel-8.1.0
            nodejs-packaging:
                rationale: RPM Macros and Utilities for Node.js Packaging
                buildorder: 100
                ref: rhel-8.0
            nodejs-nodemon:
                rationale: Simple monitor script for use during development of a node.js app
                buildorder: 200
                ref: rhel-8.0
